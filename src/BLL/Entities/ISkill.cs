﻿using BLL.Enums;

namespace BLL.Entities
{
    public interface ISkill
    {
        /// <summary>Выполняет ли сейчас умение человек</summary>
        bool IsRunning { get; set; }

        string Description { get; set; }

        SkillType SkillType { get; set; }
    }
}
