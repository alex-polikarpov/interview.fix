﻿using BLL.Enums;
using System.Collections.Generic;

namespace BLL.Entities
{
    public class NightClub: INightClub
    {
        public IEnumerable<IHuman> Visitors { get; set; }

        public MusicType ActiveMusic { get; set; }
    }
}