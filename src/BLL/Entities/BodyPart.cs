﻿using BLL.Enums;
using System;

namespace BLL.Entities
{
    public class BodyPart<TState> : IBodyPart
    {
        public BodyPart()
        {
            if (!typeof(TState).IsEnum)
            {
                throw new ArgumentException("TState должно быть перечислением");
            }
        }

        /// <summary>Текущее состояние/положение/действие части тела</summary>
        public TState State { get; set; }

        public BodyPartType BodyPartType { get; set; }

        public object GetState()
        {
            return State;
        }
    }
}