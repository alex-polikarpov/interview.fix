﻿using System.ComponentModel;
using BLL.Constants;

namespace BLL.Enums
{
    public enum GenderType : byte
    {
        [Description(StringConstants.Descriptions.Male)]
        Male = 0,

        [Description(StringConstants.Descriptions.FeMale)]
        Female = 1
    }
}