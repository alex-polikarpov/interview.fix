﻿using System.ComponentModel;
using BLL.Constants;

namespace BLL.Enums
{
    public enum HandsState : byte
    {
        None = 0,

        [Description(StringConstants.Descriptions.DrinkVodka)]
        DrinkVodka = 1,

        /// <summary>Согнутые локти</summary>
        [Description(StringConstants.Descriptions.BentElbows)]
        BentElbows = 2,

        /// <summary>Плавные движения</summary>
        [Description(StringConstants.Descriptions.SmoothMovements)]
        SmoothMovements = 3,

        /// <summary>Вращение руками</summary>
        [Description(StringConstants.Descriptions.Rotation)]
        Rotation = 4
    }
}