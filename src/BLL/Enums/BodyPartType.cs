﻿namespace BLL.Enums
{
    public enum BodyPartType : byte
    {
        Torso = 0,
        Head = 1,
        Hands = 2,
        Legs = 3
    }
}