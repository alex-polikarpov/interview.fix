﻿using System.Net;

namespace BLL.Models
{
    /// <summary>Модель для хранения статуса ответа от сервера</summary>
    public class ResponseStatusModel
    {
        public ResponseStatusModel()
        {

        }

        public ResponseStatusModel(HttpStatusCode code, string error = "")
        {
            Code = code;
            Error = error;
        }

        public HttpStatusCode Code { get; set; }

        public string Error { get; set; }
    }
}