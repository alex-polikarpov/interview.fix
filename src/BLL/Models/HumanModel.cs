﻿using System;

namespace BLL.Models
{
    public class HumanModel
    {
        public Guid Id { get; set; }

        public string Gender { get; set; }

        public string RunningSkill { get; set; }

        public string TorsoState { get; set; }

        public string HeadState { get; set; }

        public string HandsState { get; set; }

        public string LegsState { get; set; }

        public string CanDanceMusic { get; set; }
    }
}