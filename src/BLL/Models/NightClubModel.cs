﻿using System.Collections.Generic;
using BLL.Enums;

namespace BLL.Models
{
    public class NightClubModel
    {
        public List<HumanModel> Visitors { get; set; }

        public MusicType ActiveMusic { get; set; }

        public string ActiveMusicName { get; set; }

        public ResponseStatusModel ResponseStatus { get; set; }
    }
}