﻿namespace BLL.Constants
{
    public static class StringConstants
    {
        public const string NightClubCacheKey = "NightClubVisitors";

        public static class Descriptions
        {
            public const string Male = "Мужской";

            public const string FeMale = "Женский";

            public const string DrinkVodka = "Пьет водку";

            public const string BentElbows = "Согнуты в локтях";

            public const string SmoothMovements = "Плавно двигает";

            public const string Rotation = "Вращает";

            public const string BackForwardSwinging = "Покачивает назад вперед";

            public const string SmallMovements = "Слабо двигает";

            public const string Crouch = "В полуприсяде";

            public const string RhythmMovenment = "Двигает в ритме музыки";

            public const string Rnb = "Rnb";

            public const string Electrohouse = "Electrohouse";

            public const string Pop = "Pop";
        }
    }
}