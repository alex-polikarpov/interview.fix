﻿using System;
using System.Web;
using BLL.Constants;
using BLL.Entities;

namespace BLL.Other
{
    public static class CacheManager
    {
        public static INightClub GetNightClub()
        {
            var value = HttpContext.Current.Session[StringConstants.NightClubCacheKey];

            if (value != null)
            {
                try
                {
                    return (INightClub)value;
                }
                catch (Exception)
                {
                    throw new Exception("");
                }
            }

            return null;
        }

        public static void SetNightClub(object club)
        {
            HttpContext.Current.Session[StringConstants.NightClubCacheKey] = club;
        }
    }
}