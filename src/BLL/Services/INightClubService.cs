﻿using BLL.Entities;
using BLL.Enums;

namespace BLL.Services
{
    public interface INightClubService
    {
        INightClub GetNightClub();

        void ChangeMusic(MusicType musicType);
    }
}