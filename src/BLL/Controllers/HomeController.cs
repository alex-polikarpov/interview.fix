﻿using System.Net;
using BLL.Enums;
using BLL.Factories;
using BLL.ModelBuilders;
using BLL.Models;
using BLL.Services;
using System.Web.Mvc;

namespace NightClub.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var builder = new MusicListModelBuilder();
            return View(builder.Build());
        }

        [HttpPost]
        public ActionResult GetNightClub()
        {
            var humanFactory = new HumanFactory(new DanceSkillFactory());
            var humanService = new HumanService();
            var service = new NightClubService(humanFactory, humanService);

            var builder = new NightClubModelBuilder();
            var model = builder.Build(service.GetNightClub());

            return Json(model);
        }

        [HttpPost]
        public ActionResult ChangeMusic(MusicType music)
        {
            var humanFactory = new HumanFactory(new DanceSkillFactory());
            var humanService = new HumanService();
            var service = new NightClubService(humanFactory, humanService);
            service.ChangeMusic(music);
            return Json(new
                {
                    ResponseStatus = new ResponseStatusModel(HttpStatusCode.OK)
                });
        }
    }
}
