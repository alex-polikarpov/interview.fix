﻿using BLL.Entities;

namespace BLL.Factories
{
    public abstract class HumanFactoryBase : IHumanFactory
    {
        public abstract IHuman CreateRandomHuman();
    }
}