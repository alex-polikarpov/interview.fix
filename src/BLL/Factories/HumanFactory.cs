﻿using BLL.Constants;
using BLL.Entities;
using BLL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Factories
{
    public sealed class HumanFactory : HumanFactoryBase
    {
        private readonly SkillFactoryBase _skillFactory;

        public HumanFactory(SkillFactoryBase skillFactory)
        {
            _skillFactory = skillFactory;
        }

        public override IHuman CreateRandomHuman()
        {
            var r = new Random();
            var gender = (GenderType) r.Next(0, 2);
            var skillsList = new List<ISkill>();

            while (skillsList.Count < NumericConstants.RandomDanceSkillsCount)
            {
                var skill = (DanceSkill) _skillFactory.CreateRandomSkill(EntitiesConstants.DanceSkills);

                if (skillsList.FirstOrDefault(s => ((DanceSkill) s).MusicType == skill.MusicType) == null)
                {
                    skillsList.Add(skill);
                }
            }

            skillsList.Add(new DrinkSkill
                {
                    DrinkType = DrinkType.Vodka, 
                    Description = "Пьет водку",
                    SkillType = SkillType.Drink
                });

        var bodyParts = new List<IBodyPart>
                {
                    new BodyPart<TorsoState> {BodyPartType = BodyPartType.Torso, State = TorsoState.None},
                    new BodyPart<HeadState> {BodyPartType = BodyPartType.Head, State = HeadState.None},
                    new BodyPart<HandsState> {BodyPartType = BodyPartType.Hands, State = HandsState.None},
                    new BodyPart<LegsState> {BodyPartType = BodyPartType.Legs, State = LegsState.None}
                };

            var human = new Human
                {
                    Id = Guid.NewGuid(),
                    Gender = gender,
                    Skills = skillsList,
                    BodyParts = bodyParts
                };

            return human;
        }
    }
}